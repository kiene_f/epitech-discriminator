<?php

$file = fopen("datas.txt", "w");

if (count($argv) !== 2 && is_numeric($nb))
{
	echo "Nombre d'argument incorrect.\n";
	exit(84);
}

$nb = (int)$argv[1];
$e = array();
$x = rand(-200, 200);
$y = rand(-200, 200);
for ($i = 0; $i < $nb && $i < 30; $i++)
{
	$e[] = generateEllipsis(rand(100, 200), ['x' => $x, 'y' => $y], rand(30, 100), rand(30, 100));
	if ($i % 2 == 0)
		$x += rand(100, 200);
	else
		$y += rand(100, 200);
}
foreach ($e as $e1) {
	foreach ($e1 as $pt) {
		fputs($file, $pt->x.";".$pt->y."\n");
	}
}
fclose($file);

function generateEllipsis($pointsCount, $center, $width, $height)
{
	$points = [];

	for ($i = 0; $i < $pointsCount; $i++)
	{
		$phi = mt_rand(0, 2 * M_PI * 1000000) / 1000000.0;
		$rho = mt_rand(0, 1000000) / 1000000.0;

		$p = (object)[
			'x' => sqrt($rho) * cos($phi) * ($width / 2) + $center['x'],
			'y' => sqrt($rho) * sin($phi) * ($height / 2) + $center['y'],
		];
		$points[] = $p;
	}

	return ($points);
}