<?php

class Graph
{

	private $dotSize;
	private $imgWidth;
	private $imgHeight;
	private $margins;
	private $colors;
	private $bgColor;
	private $defaultColor;
	private $image;

	private $points = [];
	private $lines = [];

	public function __construct($imgWidth, $imgHeight, $dotSize = 2, $margins = 0)
	{
		$this->imgWidth = $imgWidth;
		$this->imgHeight = $imgHeight;
		$this->dotSize = $dotSize;
		$this->margins = $margins;
		$this->defaultColor = "#000000";

		$this->image = imagecreate($this->imgWidth, $this->imgHeight);
	}

	public function setPoints($points)
	{
		$this->points = $points;
	}

	public function addPoints($points)
	{
		$this->points = array_merge($this->points, $points);
	}

	public function addColor($color)
	{
		$this->getColor($color);
	}

	public function addLines($lines)
	{
		$this->lines = array_merge($this->lines, $lines);
	}

	public function setDefaultColor($color)
	{
		$this->defaultColor = $color;
	}

	public function setBackground($color, $colorName = NULL)
	{
		$color = str_replace("#", "", $color);

		$r = hexdec(substr($color, 0, 2));
		$g = hexdec(substr($color, 2, 2));
		$b = hexdec(substr($color, 4, 2));

		$colorId = !empty($colorName) ? $colorName : '#' . $color;

		$this->colors[$colorId] = imagecolorallocate($this->image, $r, $g, $b);
		$this->bgColor = $colorId;

		imagefilledrectangle($this->image, 0, 0, $this->imgWidth, $this->imgHeight, $this->colors[$colorId]);
	}

	public function render()
	{
		if (empty($_GET['debug']))
			header('Content-type: image/png');

		$points = $this->pointsToRenderPoints();
		foreach ($points as $p)
			imagefilledellipse($this->image, $p->x, $p->y, $this->dotSize, $this->dotSize, $this->getColor($p->color));

		$lines = $this->linesToRenderLines();
		foreach ($lines as $l)
			imageline($this->image, $l->x1, $l->y1, $l->x2, $l->y2, $this->getColor('default'));

		imagepng($this->image);
	}

	private function pointsToRenderPoints()
	{
		$xRange = $this->getRange('x');
		$yRange = $this->getRange('y');

		$renderPoints = [];
		foreach ($this->points as $d)
		{
			if (property_exists($d, "points"))
			{
				foreach ($d->points as $p)
				{
					$xPercent = ($p->x - $xRange['min']) / $xRange['range'] * 100;
					$yPercent = ($p->y - $yRange['min']) / $yRange['range'] * 100;

					$renderPoints[] = (object)[
						 'x' => intval(($this->imgWidth - $this->margins * 2) * $xPercent / 100) + $this->margins,
						 'y' => ($this->imgHeight - $this->margins * 2) - intval(($this->imgHeight - $this->margins * 2) * $yPercent / 100) + $this->margins,
						 'color' => $d->color,
					];
				}
			}
			else
			{
				$xPercent = ($d->x - $xRange['min']) / $xRange['range'] * 100;
				$yPercent = ($d->y - $yRange['min']) / $yRange['range'] * 100;

				$renderPoints[] = (object)[
					'x' => intval(($this->imgWidth - $this->margins * 2) * $xPercent / 100) + $this->margins,
					'y' => ($this->imgHeight - $this->margins * 2) - intval(($this->imgHeight - $this->margins * 2) * $yPercent / 100) + $this->margins,
					'color' => !empty($d->color) ? $d->color : 'default',
				];
			}
		}

		return ($renderPoints);
	}

	private function linesToRenderLines()
	{
		$xRange = $this->getRange('x');
		$yRange = $this->getRange('y');

		$renderLines = [];
		foreach ($this->lines as $d)
		{
			$x1Percent = ($d->x1 - $xRange['min']) / $xRange['range'] * 100;
			$x2Percent = ($d->x2 - $xRange['min']) / $xRange['range'] * 100;
			$y1Percent = ($d->y1 - $yRange['min']) / $yRange['range'] * 100;
			$y2Percent = ($d->y2 - $yRange['min']) / $yRange['range'] * 100;

			$renderLines[] = (object)[
				'x1' => intval(($this->imgWidth - $this->margins * 2) * $x1Percent / 100) + $this->margins,
				'x2' => intval(($this->imgWidth - $this->margins * 2) * $x2Percent / 100) + $this->margins,
				'y1' => ($this->imgHeight - $this->margins * 2) - intval(($this->imgHeight - $this->margins * 2) * $y1Percent / 100) + $this->margins,
				'y2' => ($this->imgHeight - $this->margins * 2) - intval(($this->imgHeight - $this->margins * 2) * $y2Percent / 100) + $this->margins,
			];
		}

		return ($renderLines);
	}

	private function getRange($axe)
	{
		$min = $max = NULL;

		foreach ($this->points as $d)
		{
			if (property_exists($d, "points"))
			{
				foreach ($d->points as $p)
				{
					$min = $min < $p->{$axe} && $min != NULL ? $min : $p->{$axe};
					$max = $max > $p->{$axe} && $max != NULL ? $max : $p->{$axe};
				}
			}
			else
			{
				$min = $min < $d->{$axe} && $min != NULL ? $min : $d->{$axe};
				$max = $max > $d->{$axe} && $max != NULL ? $max : $d->{$axe};
			}
		}

		return ([
			'max' => $max,
			'min' => $min,
			'range' => $max - $min,
		]);
	}

	private function getColor($colorId)
	{
		if ($colorId == 'random')
		{
			if (count($this->colors) == 1)
			{
				return ($this->colors[array_keys(array_slice($this->colors, 0, 1))[0]]);
			}

			$colorId = $this->bgColor;
			while ($colorId == $this->bgColor)
			{
				$colorId = array_slice(array_keys($this->colors), rand(0, count($this->colors) - 1), 1)[0];
			}

			return ($this->colors[$colorId]);
		}
		else if ($colorId == 'default')
		{
			if (!in_array($colorId, array_keys($this->colors)))
			{
				$color = str_replace("#", "", $this->defaultColor);

				$r = hexdec(substr($color, 0, 2));
				$g = hexdec(substr($color, 2, 2));
				$b = hexdec(substr($color, 4, 2));

				$this->colors[$colorId] = imagecolorallocate($this->image, $r, $g, $b);
			}

			return ($this->colors[$colorId]);
		}
		else
		{
			if (!in_array($colorId, array_keys($this->colors)))
			{
				$color = str_replace("#", "", $colorId);

				$r = hexdec(substr($color, 0, 2));
				$g = hexdec(substr($color, 2, 2));
				$b = hexdec(substr($color, 4, 2));

				$this->colors[$colorId] = imagecolorallocate($this->image, $r, $g, $b);
			}

			return ($this->colors[$colorId]);
		}
	}

}
