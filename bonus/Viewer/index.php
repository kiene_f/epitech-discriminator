<?php
function __autoload($class_name)
{
	require_once $class_name . '.class.php';
}

$file = "../Examples/datas.txt";

if (($pts = readPoints($file)) === null)
{
	echo ("Le fichier ".$file." presente une erreur.");
	exit(84);
}

$mdist = averageDistances($pts) * 1.25;

$zones = getZones($pts, $mdist);

$totalzone = countNumberOfZone($zones, $mdist);

display($zones);

exit(0);

function readPoints($file)
{
	$pts = array();
	if (!file_exists($file))
		return (null);
	$file = fopen($file, "r");
	$i = 0;
	while ($line = fgets($file))
	{
		$line = substr($line, 0, strlen($line) - 2);
		if (!empty($line))
		{
			$tmp = explode(';', $line, 2);
			if (count($tmp) === 2 && is_numeric($tmp[0]) == true && is_numeric($tmp[1]) == true)
			{
				$pt = new Point($line, $i);
				$pts[] = $pt;
				$i++;
			}
			else
				return (null);
		}
		else
			return (null);
	}
	fclose($file);
	if (count($pts) > 0)
		return ($pts);
	else
		return (null);
}

function averageDistances($pts)
{
	$pt2 = $pts[0];
	$mdist = 0;
	$i = 0;
	foreach($pts as $pt)
	{
		$mdist += sqrt(pow(($pt2->x - $pt->x), 2) + pow(($pt2->y - $pt->y), 2));
		$pt2 = $pt;
		$i++;
	}

	$mdist = ($mdist / $i);
	return ($mdist);
}

function getZones($pts, $mdist)
{
	$nbpts = count($pts);
	$interval = ceil($nbpts / 750);
	$zones = array(array());

	for ($i = 0; $i < $nbpts; $i += $interval)
	{
		for ($j = $i; $j < $nbpts; $j += $interval)
		{
			if ($pts[$j]->zone != $pts[$i]->zone && 
				abs($pts[$j]->x - $pts[$i]->x) < $mdist && abs($pts[$j]->y - $pts[$i]->y) < $mdist 
				&& sqrt(pow(($pts[$j]->x - $pts[$i]->x), 2) + pow(($pts[$j]->y - $pts[$i]->y), 2)) < $mdist)
			{
				unset($zones[$pts[$j]->zone][$j]);
				$zones[$pts[$i]->zone][$j] = $pts[$j];
				$pts[$j]->zone = $pts[$i]->zone;
			}
		}
	}
	return ($zones);
}

function countNumberOfZone($zones, $mdist)
{
	$totalzone = 0;
	foreach ($zones as $zone)
	{
		if (count($zone) <= 5)
			continue;

		$i = 0;
		$totalzone += 1;
		foreach ($zone as $pt) {
			if ($i === 0)
			{
				$maxX = new Point($pt->x, $pt->y, 0);
				$maxY = new Point($pt->x, $pt->y, 0);
				$minX = new Point($pt->x, $pt->y, 0);
				$minY = new Point($pt->x, $pt->y, 0);
				$i++;
				continue;
			}
			if ($pt->x > $maxX->x)
				$maxX = $pt;
			else if ($pt->x < $minX->x)
				$minX = $pt;
			else if ($pt->y > $maxY->y)
				$maxY= $pt;
			else if ($pt->y < $minY->y)
				$minY = $pt;
			$i++;
		}
		$t = $totalzone;
		$save = $maxY;
		$save2 = $maxX;
		while (($save = check($save, $minX, $zone)) !== $minX &&
			($save2 = check($save2, $minY, $zone)) !== $minY)
			$totalzone += 1;
		if ($t !== $totalzone)
			continue;
		$save = $maxX;
		$save2 = $minX;
		while (($save = check($save, $maxY, $zone)) !== $maxY &&
			($save2 = check($save2, $minY, $zone)) !== $minY)
			$totalzone += 1;
	}
	
	return ($totalzone);
}

function check($pt1, $pt2, $zone)
{
	$pts = array();
	$dsize = sqrt(pow($pt2->x - $pt1->x, 2) + pow($pt2->y - $pt1->y, 2));

	$pts[] = new Point(($pt1->x+$pt2->x)/2, ($pt1->y+$pt2->y)/2, 0);
	$pts[] = new Point(($pt1->x+$pts[0]->x)/2, ($pt1->y+$pts[0]->y)/2, 0);
	$pts[] = new Point(($pts[0]->x+$pt2->x)/2, ($pts[0]->y+$pt2->y)/2, 0);
	$pts[] = new Point(($pts[0]->x+$pts[1]->x)/2, ($pts[0]->y+$pts[1]->y)/2, 0);
	$pts[] = new Point(($pts[0]->x+$pts[2]->x)/2, ($pts[0]->y+$pts[2]->y)/2, 0);


	$pt3 = null;
	$mdist = 0;
	$i = 0;
	foreach($zone as $pt)
	{
		if ($i > 0)
			$mdist += sqrt(pow(($pt3->x - $pt->x), 2) + pow(($pt3->y - $pt->y), 2));
		$pt3 = $pt;
		$i++;
	}

	$tab = array();
	$m = 0;
	$mdist = ($mdist / $i) / 2;
	foreach ($pts as $mid) {
		$nbpts = 0;
		foreach ($zone as $pt) {
			if (($pt->x <= ($mid->x + $mdist) && $pt->x >= ($mid->x - $mdist)) && 
				($pt->y <= ($mid->y + $mdist) && $pt->y >= ($mid->y - $mdist)))
				$nbpts += 1;
		}
		$tab[] = $nbpts;
		$m += $nbpts;
	}

	$m = $m / 5;
	$i = 0;
	$j = 0;
	$k = 0;
	foreach($tab as $pt)
	{
		if ($pt + 2 < ($m / 2))
		{
			$j = $k;
			$i++;
		}
		$k++;
	}
	if ($i > 0)
		return ($pts[$j]);
	return ($pt2);
}

function display($zones)
{
	$graph = new Graph(800, 800, 2, 50);

	$graph->setBackground('#000000', 'background');
	$graph->setDefaultColor('#ffffff');
	$graph->addColor('#ffb400');
	$graph->addColor('#54ff00');
	$graph->addColor('#00f6ff');

	$nbpts = count($zones);
	foreach ($zones as $zone) {
		if (count($zone) > 5)
		foreach ($zone as $pt) {
			$graph->addPoints([
				 (object)[
					  'color' => RGBToHex($pt->zone / ($nbpts) * 255, 255 - ($pt->zone / ($nbpts) * 255), $pt->zone / ($nbpts) * 255),
					  'points' => [(object)['x' => $pt->x, 'y' => $pt->y]]
						]
			]);
		}
	}

	$graph->render();
}

function RGBToHex($r, $g, $b) 
{
	$hex = "#";
	$hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
	$hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
	$hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
 
	return $hex;
}