<?php

class Point
{

	private $m_x;
	private $m_y;
	private $zone;

	private $rdonly = ['x', 'y'];

	public function __construct()
	{
		$args = func_get_args();
		if (count($args) == 2)
		{
			$str = str_replace("\n", "", $args[0]);
			if (count(explode(';', $str, 2)) === 2)
			{
				$this->m_x = floatval(explode(';', $str, 2)[0]);
				$this->m_y = floatval(explode(';', $str, 2)[1]);
				$this->zone = $args[1];
			}
		}
		else if (count($args) == 3)
		{
			$this->m_x = floatval($args[0]);
			$this->m_y = floatval($args[1]);
			$this->zone = $args[2];
		}
	}

	public function __get($name)
	{
		if (property_exists($this, 'm_' . $name))
			return ($this->{'m_' . $name});
		else if (property_exists($this, $name))
			return ($this->{$name});
		else
			throw new Exception("Invalid parameter");
	}

	public function __set($name, $value)
	{
		//if (in_array($name, $this->rdonly))
			//throw new Exception("This parameter is readonly");
		if (property_exists($this, $name))
			$this->{$name} = $value;
		else if (property_exists($this, 'm_' . $name))
			$this->{'m_' . $name} = $value;
	}

	public function isInZone(Zone $zone)
	{
		return ($this->x >= $zone->x1 && $this->x <= $zone->x2
				  && $this->y >= $zone->y1 && $this->y <= $zone->y2);
	}

}
