Discriminator
===================

Ce dépôt contient les sources du Discriminator de mon groupe pour le module programmation appliquée de 3e année à Epitech.
Il a été réalisé à Strasbourg en 2016.

----------

Fonctionnalités
-------------

> - Résolution des nuages de points
> - Affichage graphique des nuages de points
> - Générateur de nuages de points

Disclaimer
-------------

Si vous êtes étudiant à Epitech, servez vous des concepts utilisés, pas du code, ou la moulinette de triche vous trouvera et vous mettra un -42.